// const { $ } = require("protractor");

$(document).ready(()=>{
    // animaton images
    // main-section
    $(".view-field").mouseover((e)=>{
        $(e.currentTarget).parents(".col-xl-4").addClass("filter");
        $(".col-xl-4").siblings().css({"opacity":"0.3","transition":"300ms ease-in-out"});
    })
    $(".view-field").mouseout((e)=>{
        $(e.currentTarget).parents(".col-xl-4").removeClass("filter");
        $(".col-xl-4").siblings().css({"opacity":"1","transition":"300ms ease-in-out"});
    })
    // upcoming section
    $(".upcoming-movie-field").mouseover((e)=>{
        $(e.currentTarget).parents(".col-xl-3").addClass("filter");
        $(".col-xl-3").siblings().css({"opacity":"0.3","transition":"300ms ease-in-out"});
    })
    $(".upcoming-movie-field").mouseout((e)=>{
        $(e.currentTarget).parents(".col-xl-3").removeClass("filter");
        $(".col-xl-3").siblings().css({"opacity":"1","transition":"300ms ease-in-out"});
    })
})